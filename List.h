//#ifndef _LIST_H_INCLUDE_
#define _LIST_H_INCLUDE_

typedef struct ListObj* List;
typedef struct NodeObj* Node;
// Constructors-Destructors ---------------------------------------------------

List newList(void); // Creates and returns a new empty List
Node newNode(int);

void freeList(List* pL); // Frees all heap memory associated with *pL, and sets

int length(List L); // Returns the number of elements in L

int index(List L); // Returns index of cursor element if defined

int front(List L); // Returns front element of L
int back(List L); // Returns back element of L

int get(List L); // Returns cursor element of L

int equals(List A, List B); // Returns true iff Lists A and B are in same


void clear(List L); // Resets L to its original empty state.

void moveFront(List L); // If L is non-empty, sets cursor under the front element
 
void moveBack(List L); // If L is non-empty, sets cursor under the back element

void movePrev(List L); // If cursor is defined and not at front, move cursor

void moveNext(List L); // If cursor is defined and not at back, move cursor

void prepend(List L, int data); // Insert new element into L. If L is non-empty

void append(List L, int data); // Insert new element into L. If L is non-empty

void insertBefore(List L, int data); // Insert new element before cursor

void insertAfter(List L, int data); // Insert new element before cursor

void deleteFront(List L); // Delete the front element.

void deleteBack(List L); // Delete the back element.

void delete(List L); // Delete cursor element, making cursor undefined

void printList(FILE* out, List L); // Prints to the file pointed to by out

List copyList(List L); // Returns a new List representing the same integer

